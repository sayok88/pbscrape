import json

from wbdrvr import *
import time
import  pandas as pd
import requests


def get_venue_dates(venue):
    resp = requests.get(get_venue_dates_url.format(venue,'yes'))
    data = resp.json()
    for dates in data['dates']:
        get_result(dates, venue)

def get_result(date_str, venue):
    data = {'venue':venue, 'date': date_str, 'results':[]}
    driver.get(result_url.format(venue, date_str))
    time.sleep(1)  # sleep_between_interactions
    elms = driver.find_elements_by_class_name('filter_whole_div')
    if len(elms):
        elms = elms[0]
    else:
        return
    racesh5 = elms.find_elements_by_class_name('pagination_filter_div')
    race_div_ids = []
    for raceh5 in racesh5:
        race_div_ids.append(raceh5.find_element_by_tag_name('a').get_attribute('href').split('#')[1])
    for div_id in race_div_ids:
        race_data = {'race_id': div_id}
        result_div = driver.find_element_by_id(div_id)
        rows = result_div.find_elements_by_class_name('row')
        if len(rows):
            result_head = rows[0]
            side_num = result_head.find_elements_by_class_name('side_num')[0].text
            race_data.update({'race_num': side_num})
            center_head = result_head.find_elements_by_class_name('center_heading')[0]
            center_name = center_head.find_elements_by_tag_name('h2')[0].text
            race_data.update({'race_name': center_name})
            center_desc = center_head.find_elements_by_tag_name('h3')[0].text
            race_data.update({'race_desc': center_desc})
            archive_time = result_head.find_elements_by_class_name('archive_time')[0].find_elements_by_tag_name('h4')
            race_length = archive_time[0].text
            race_time = archive_time[1].text
            race_data.update({'race_length': race_length})
            race_data.update({'race_time': race_time})
            winner_head = result_div.find_elements_by_class_name('winner_row')[0]
            winner_content = winner_head.find_elements_by_class_name('winner_content')[0].find_elements_by_tag_name('span')
            winner_content = [win.text for win in winner_content]
            race_data.update({'winner_content': winner_content})
            main_content = result_div.find_elements_by_class_name('margin_30')[0]
            result_table = main_content.find_elements_by_tag_name('table')[0]
            result_pd = pd.read_html(result_table.get_attribute('outerHTML'))
            result_json = result_pd[0].to_dict('records')
            race_data.update({'result': result_json})
            race_data1_elm = main_content.find_elements_by_class_name('row')[0].find_elements_by_tag_name('span')[0]
            race_data1 = race_data1_elm.text
            race_data.update({'race_data1': race_data1})
            race_video = race_data1_elm.find_elements_by_tag_name('a')
            if race_video:
                race_video = race_video[0].get_attribute('href')
            else:
                race_video = ''
            race_data.update({'race_video': race_video})
            race_tables = main_content.find_elements_by_class_name('row')[0].find_elements_by_tag_name('table')
            bets = []
            for t in race_tables:
                result_pd = pd.read_html(t.get_attribute('outerHTML'))
                bets.append(result_pd[0].to_dict('records'))
            race_data.update({'bets': bets})
            racing_events = main_content.find_elements_by_class_name('racingCenterEvent-table')
            racing_event_data = {}
            if len(racing_events):
                racing_event_data = pd.read_html(racing_events[0].get_attribute('outerHTML'))[0].to_dict('records')
            race_data.update({'racing_events': racing_event_data})

        data['results'].append(race_data)
    try:
        resp = requests.post(process_result_url, data=json.dumps(data) ,headers={"Content-Type":"application/json"})
        print(resp.json())

    except Exception as e:
        print(data)
        print(e)
        print(result_url.format(venue, date_str))


# def sayok(func):
#     def inner(*args, **kwargs):
#         print('my name')
#
#         func(*args, **kwargs)
#     return inner
#
#
# @sayok
# def abc():
#     pass



if __name__ == '__main__':
    get_venue_dates(1)
    # get_result('2021-08-21',11)
    driver.quit()
    # abc()