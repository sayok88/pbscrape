# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.
import json
import os

import requests

import time
from wbdrvr import *
from webdriver_manager.chrome import ChromeDriverManager
from selenium.common.exceptions import ElementClickInterceptedException
from datetime import datetime
from venues import Venues


day_button = '/html/body/div/div[2]/div[3]/div/div[3]/div[1]/div[2]/div/button[3]'
back_arrow = '/html/body/div/div[2]/div[3]/div/div[3]/div[1]/div[1]/div/button[1]/span'
event_button = '/html/body/div/div[2]/div[3]/div/div[3]/div[2]/div/table/tbody/tr/td/div/div/div/div[3]/table/tbody/tr/td'
date_head = '/html/body/div/div[2]/div[3]/div/div[3]/div[1]/div[3]/h2'


def initial_try(name):
    # Use a breakpoint in the code line below to debug your script.
    driver.get(archive_url.format(Venues.IDS.get(name)))
    time.sleep(5)  # sleep_between_interactions
    day_button_el = driver.find_element_by_xpath(day_button)
    day_button_el.click()
    dates = []
    def nav_through():
        # time.sleep(0.2)  # sleep_between_interactions
        back_arrow_el = driver.find_element_by_xpath(back_arrow)
        back_arrow_el.click()
    for _ in range(365*14):
    # for _ in range(100):

        nav_through()
        try:
            driver.find_element_by_xpath(event_button)
        except:
            continue
        try:
            date = driver.find_element_by_xpath(date_head)
            date_str = date.get_attribute('innerText')
            current_date = datetime.strptime(date_str,'%B %d, %Y')
            dates.append(current_date.strftime("%Y-%m-%d"))
            # resp = requests.get(venue_dates_url.format(current_date.strftime("%Y-%m-%d"), ))
            # print(resp.json())
        except Exception as e:
            print(e)
    data ={'dates':dates}
    print(data)
    resp = requests.post(store_dates_urls.format(Venues.IDS.get(name)),
                                                 data=json.dumps(data),
                         headers={"Content-Type":"application/json"})
    print(resp.json())


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    for venue in Venues.IDS:
        initial_try(venue)

    driver.close()



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
