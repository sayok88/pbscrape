import requests
from decouple import config
result_url = 'https://www.indiarace.com/Home/racingCenterEvent?venueId={}&event_date={}&race_type=RESULTS'
archive_url = 'https://www.indiarace.com/Home/raceArchives?venueId={}'

# site_base_url = 'https://peaky-blinder.herokuapp.com' # change this

site_base_url = 'http://localhost:8000' # change this

get_token_url = site_base_url+'/api/api-token-auth/'
def get_token(u,p):
    resp = requests.post(get_token_url, data={'username': u, 'password':p})
    res = resp.json()
    return res.get('token')
token = get_token(config('UNAME'), config('PASSWORD'))
print(token)
get_venue_dates_url = site_base_url+'/api/getvenuedates/?venue={}&ee={}&token='+token
get_venue_dates_existing_url = site_base_url+'/api/getvenuedates/?venue={}&token='+token
process_result_url = site_base_url+'/api/process_result/?token='+token
venue_dates_url = site_base_url+'/api/racedate/?date={}&venue={}&token='+token
get_result_url = site_base_url+'/api/get_result/?date={}&venue={}&token='+token
store_dates_urls = site_base_url+'/api/store_dates/?venue={}&token='+token
