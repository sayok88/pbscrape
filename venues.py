class Venues:
    HYDERABAD = 'Hyderabad'
    KOLKATA = 'Kolkata'
    PUNE = 'Pune'
    OOTY_CHENNAI = 'Ooty-Chennai'
    MYSORE = 'Mysore'
    DELHI = 'Delhi'
    CHENNAI = 'Chennai'
    MUMBAI = 'Mumbai'
    BANGALORE = 'Bangalore'
    IDS = {HYDERABAD: 11,
           KOLKATA: 1,
           MUMBAI: 2,
           BANGALORE: 3,
           CHENNAI: 4,
           DELHI: 7,
           MYSORE: 8,
           OOTY_CHENNAI: 9,
           PUNE: 10}
