import requests

from constants import get_result_url, get_venue_dates_existing_url


def fetch_result(venue):
    resp = requests.get(get_venue_dates_existing_url.format(venue))
    data = resp.json()
    for dates in data['dates']:
        print(get_results(dates, venue))

def get_results(date, venue):
    resp = requests.get(get_result_url.format(date, venue))
    return resp.json()


if __name__ == '__main__':
    fetch_result(11)