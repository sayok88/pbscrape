"""
Use this program to all load data into django app
"""
from get_results import get_venue_dates
from get_venue_dates import initial_try
from venues import Venues
from wbdrvr import driver

def load_data():
    for venue in Venues.IDS:
        initial_try(venue)
    for venue in Venues.IDS:
        get_venue_dates(Venues.IDS.get(venue))
    driver.quit()

if __name__=='__main__':
    load_data()